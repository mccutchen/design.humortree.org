            <table border="0" cellpadding="0" cellspacing="5" align="center">
						<tr>
							<td align="right" class="sidebarText">Skills |</td>
							<td align="left" class="sidebarText"><a href="?section=us&subsection=design">Design</a></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" class="sidebarText"><a href="javascript:void(null);">Programming</a></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" class="sidebarText"><a href="javascript:void(null);">Architecture</a></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" class="sidebarText"><a href="javascript:void(null);">Copywriting</a></td>
						</tr>
						<tr>
							<td class="sidebarText">&nbsp;</td>
							<td class="sidebarText">&nbsp;</td>
						</tr>
            
            <tr>
							<td align="right" class="sidebarText">Proprietary |</td>
							<td align="left" class="sidebarText"><a href="?section=us&subsection=HTpost">HTpost</a></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" class="sidebarText"><a href="?section=us&subsection=HTLIB">HTLIB</a></td>
						</tr>
						<tr>
							<td class="sidebarText">&nbsp;</td>
							<td class="sidebarText">&nbsp;</td>
						</tr>
						
						<tr>
							<td align="right" class="sidebarText">Contact |</td>
							<td align="left" class="sidebarText"><a href="mailto:coalition@humortree.com">email us</a></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" class="sidebarText">or</td>
						</tr>
						<tr>
							<td></td>
							<td align="left" class="sidebarText">(903) 663 4491</td>
						</tr>
						<tr>
							<td></td>
							<td align="left" class="sidebarText">122 Oak Isle Dr.</td>
						</tr>
						<tr>
							<td></td>
							<td align="left" class="sidebarText">Longview, TX 75605</td>
						</tr>
					</table>